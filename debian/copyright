Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: geometry
Upstream-Contact: David C. Sterratt <david.c.sterratt@ed.ac.uk>
Source: https://cran.r-project.org/package=geometry
#Files-Excluded: src/g* src/i* src/l* src/m* src/p* src/q* src/r* src/s* src/u*
Comment: Currently, it is not possible to remove the embedded qhull library
 because the embedded version is 2018.0.1 whereas the Debian version is 2015.2,
 this leads among others to build failures.

Files: *
Copyright: 2012-2015 C. B. Barber, Kai Habel, Raoul Grasman, Robert B. Gramacy,
                     Andreas Stahel, David C. Sterratt
License: GPL-3+

Files: src/g*
       src/i*
       src/l*
       src/m*
       src/p*
       src/q*
       src/r*
       src/s*
       src/u*
Copyright: 1993-2015 The Geometry Center, C.B. Barber
License: GPL-3+

Files: debian/*
Copyright: 2018 Andreas Tille <tille@debian.org>
License: GPL-3+

License: GPL-3+
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
 On Debian systems you can find a full copy of the GNU General Public
 License version 3 at /usr/share/common-licenses/GPL-3.
